"""
Registration of a user 0 tokens
Each user gets 10 tokens
Store a sentence on our database for 1 token
Retrieve his stored sentence on out database for 1 token

https://towardsdatascience.com/working-with-apis-using-flask-flask-restplus-and-swagger-ui-7cf447deda7f

usage  localhost:5000/api/get
Swagger docs http://127.0.0.1:5000/

"""
from flask import Flask, jsonify, request
from flask_restplus import Api, Resource, Namespace, fields
from pymongo import MongoClient
import bcrypt

app = Flask(__name__)
api = Api(app=app, version="1.0", title="Sentence Persistence API",
          description="Persist sentences for a given user.")

name_space = api.namespace('api', description='Main APIs')

client = MongoClient("mongodb://db:27017")
db = client.SentencesDatabase
users = db["Users"]

register_model = api.model('Register User Model',
                           {
                               'username': fields.String(
                                   requried=True,
                                   description="Username of the user",
                                   help="username cannot be blank"),

                               'password': fields.String(
                                   requried=True,
                                   description="User password",
                                   help="password cannot be blank")
                           })


@name_space.route("/register")
class Register(Resource):

    @api.doc(responses={200: 'OK',
                        400: 'Invalid Argument',
                        500: 'Mapping Key Error'
                        })
    @api.expect(register_model)
    def post(self):
        # Step 1 is to get posted data by the user
        postedData = request.get_json(force=True)

        # Get the data
        username = postedData["username"]
        password = postedData["password"]

        hashed_pw = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())

        # Store username and pw into the database
        users.insert({
            "Username": username,
            "Password": hashed_pw,
            "Sentence": "",
            "Tokens": 6
        })

        retJson = {
            "status": 200,
            "msg": "You successfully signed up for the API"
        }
        return jsonify(retJson)


def verifyPw(username, password):
    hashed_pw = users.find({
        "Username": username
    })[0]["Password"]

    if bcrypt.hashpw(password.encode('utf8'), hashed_pw) == hashed_pw:
        return True
    else:
        return False


def countTokens(username):
    tokens = users.find({
        "Username": username
    })[0]["Tokens"]
    return tokens


store_model = api.model('Store User Model',
                        {
                            'username': fields.String(
                                requried=True,
                                description="Username of the user",
                                help="username cannot be blank"),

                            'password': fields.String(
                                requried=True,
                                description="User password",
                                help="password cannot be blank"),
                            'sentence': fields.String(
                                requried=False,
                                description="Sentence you want to persist",
                                help="The sentence to be saved"
                            )
                        })


@name_space.route("/store")
class Store(Resource):
    @api.doc(responses={200: 'OK',
                        400: 'Invalid Argument',
                        500: 'Mapping Key Error',
                        301: 'Insufficient Token Error',
                        302: 'Bad Password Error'
                        })
    @api.expect(store_model)
    def post(self):
        # Step 1 get the posted data
        postedData = request.get_json(force=True)

        # Step 2 is to read the data
        username = postedData["username"]
        password = postedData["password"]
        sentence = postedData["sentence"]

        # Step 3 verify the username pw match
        correct_pw = verifyPw(username, password)

        if not correct_pw:
            retJson = {
                "status": 302
            }
            return jsonify(retJson)
        # Step 4 Verify user has enough tokens
        num_tokens = countTokens(username)
        if num_tokens <= 0:
            retJson = {
                "status": 301
            }
            return jsonify(retJson)

        # Step 5 store the sentence, take one token away  and return 200OK
        users.update({
            "Username": username
        }, {
            "$set": {
                "Sentence": sentence,
                "Tokens": num_tokens  # -1
            }
        })

        retJson = {
            "status": 200,
            "msg": "Sentence saved successfully"
        }
        return jsonify(retJson)


get_model = api.model('Get User Model',
                      {
                          'username': fields.String(
                              requried=True,
                              description="Username of the user",
                              help="username cannot be blank"),

                          'password': fields.String(
                              requried=True,
                              description="User password",
                              help="password cannot be blank")
                      })


@name_space.route("/get")
class Get(Resource):
    @api.doc(responses={200: 'OK',
                        400: 'Invalid Argument',
                        500: 'Mapping Key Error',
                        301: 'Insufficient Token Error',
                        302: 'Bad Password Error'
                        })
    @api.expect(get_model)
    def post(self):
        postedData = request.get_json(force=True)

        username = postedData["username"]
        password = postedData["password"]

        # Step 3 verify the username pw match
        correct_pw = verifyPw(username, password)
        if not correct_pw:
            retJson = {
                "status": 302
            }
            return jsonify(retJson)

        num_tokens = countTokens(username)
        if num_tokens <= 0:
            retJson = {
                "status": 301
            }
            return jsonify(retJson)

        # MAKE THE USER PAY!
        # users.update({
        #     "Username": username
        # }, {
        #     "$set": {
        #         "Tokens": num_tokens-1
        #     }
        # })

        sentence = users.find({
            "Username": username
        })[0]["Sentence"]
        retJson = {
            "status": 200,
            "sentence": str(sentence)
        }

        return jsonify(retJson)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
